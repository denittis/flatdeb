#!/usr/bin/python3

# flatdeb — build Flatpak runtimes from Debian packages
#
# Copyright © 2016-2017 Simon McVittie
# Copyright © 2017-2018 Collabora Ltd.
#
# SPDX-License-Identifier: MIT
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

"""
Fetch detached debug symbols for packages installed in the given sysroot.
"""

import argparse
import logging
import os
import subprocess
import sys

try:
    import typing
except ImportError:
    pass
else:
    typing  # silence "unused" warnings

from debian.deb822 import Packages


logger = logging.getLogger('flatdeb.collect-dbgsym')


class InstalledPackage:
    def __init__(self, fields):
        # type: (typing.Sequence[str]) -> None
        self.qualified_binary = fields[0]
        self.binary = self.qualified_binary.split(':')[0]
        self.binary_version = fields[1]
        self.source = fields[2] or self.binary

        if ':' not in self.qualified_binary:
            self.architecture = None    # type: typing.Optional[str]
        else:
            self.architecture = self.qualified_binary.split(':')[-1]

    def __str__(self):
        # type: () -> str
        return '{}_{}'.format(self.qualified_binary, self.binary_version)

    def __hash__(self):
        # type: () -> int
        return hash(self.qualified_binary) ^ hash(self.binary_version)

    def __eq__(self, other):
        # type: (typing.Any) -> bool
        if isinstance(other, InstalledPackage):
            return (
                self.qualified_binary,
                self.binary_version,
                self.source,
            ) == (
                other.qualified_binary,
                other.binary_version,
                other.source,
            )
        else:
            return NotImplemented

    @property
    def dbgsym_package(self):
        return self.binary + '-dbgsym'

    @property
    def qualified_dbgsym_package(self):
        # type: () -> str

        if ':' in self.qualified_binary:
            return self.qualified_binary.replace(':', '-dbgsym:')
        else:
            return self.qualified_binary + '-dbgsym'


def read_manifest(path):
    # type: (str) -> typing.List[InstalledPackage]

    ret = []

    with open(path, encoding='utf-8') as reader:
        for line in reader:
            line = line.rstrip('\n')

            if not line:
                continue

            if line.startswith('#'):
                continue

            assert '\t' in line, repr(line)
            ret.append(InstalledPackage(line.rstrip('\n').split('\t')))

    return ret


def main():
    # type: (...) -> None
    parser = argparse.ArgumentParser(
        description='Install corresponding -dbgsym packages if available',
    )
    parser.add_argument('--primary-architecture', default=None)
    parser.add_argument('--debug', action='store_true', default=False)
    parser.add_argument('sysroot')

    args = parser.parse_args()

    in_chroot = [
        'systemd-nspawn',
        '--directory={}'.format(args.sysroot),
        '--as-pid2',
        '--tmpfs=/run/lock',
        '--register=no',
    ]
    in_chroot_quick = [
        'chroot', args.sysroot,
    ]

    in_chroot.append('env')

    for var in ('ftp_proxy', 'http_proxy', 'https_proxy', 'no_proxy'):
        if var in os.environ:
            in_chroot.append('{}={}'.format(var, os.environ[var]))

    platform_manifest = os.path.join(
        args.sysroot, 'usr', 'manifest.dpkg.platform')

    if os.path.exists(platform_manifest):
        platform_packages = read_manifest(platform_manifest)
    else:
        platform_packages = []

    to_inspect = set()      # type: typing.Set[str]

    for want in platform_packages:
        logger.info('Package in Platform: %s from %s', want, want.source)
        to_inspect.add(
            '{}={}'.format(want.qualified_dbgsym_package, want.binary_version))

    if args.primary_architecture is None:
        args.primary_architecture = subprocess.run(
            in_chroot_quick + ['dpkg', '--print-architecture'],
            stdout=subprocess.PIPE,
        ).stdout.decode('ascii').strip()

    result = subprocess.run(
        in_chroot_quick + ['apt-cache', 'show'] + list(to_inspect),
        stdout=subprocess.PIPE,
    )

    binaries = []       # type: typing.List[Packages]
    to_get = set()      # type: typing.Set[str]

    for binary_stanza in Packages.iter_paragraphs(
        sequence=result.stdout.splitlines(keepends=True),
        encoding='utf-8',
    ):
        if binary_stanza['Package'].endswith('-dbgsym'):
            binaries.append(binary_stanza)

    parent = os.path.join(
        args.sysroot, 'usr', 'lib', 'debug',
    )
    os.makedirs(parent, exist_ok=True)

    with open(
        os.path.join(parent, 'dbgsym-packages-installed.txt'),
        'w',
    ) as writer, open(
        os.path.join(parent, 'dbgsym-packages-not-installed.txt'),
        'w',
    ) as missing_writer:
        for want in platform_packages:
            same_name = []      # type: typing.List[Packages]

            for binary_stanza in binaries:
                if want.dbgsym_package == binary_stanza['Package']:
                    same_name.append(binary_stanza)

            if not same_name:
                logger.info(
                    'Skipped nonexistent -dbgsym package: %s',
                    want.dbgsym_package,
                )
                continue

            for binary_stanza in same_name:
                source = binary_stanza.get(
                    'Source',
                    binary_stanza['Package'],
                )

                # It's important to check this to avoid including debug
                # symbols that would require new source code that is not
                # already required by the non-debug packages
                if want.source != source:
                    continue

                if want.binary_version != binary_stanza['Version']:
                    continue

                if (
                    want.architecture is not None
                    and want.architecture != args.primary_architecture
                    and binary_stanza.get('Multi-Arch', 'no') != 'same'
                ):
                    logger.info(
                        'Not installing %s: not Multi-Arch: same even '
                        'though the library is',
                        want.qualified_dbgsym_package,
                    )
                    continue

                logger.info(
                    'Installing -dbgsym package: %s_%s',
                    want.qualified_dbgsym_package,
                    want.binary_version,
                )
                writer.write('{}\t{}\n'.format(
                    want.qualified_dbgsym_package,
                    want.binary_version,
                ))
                to_get.add('{}={}'.format(
                    want.qualified_dbgsym_package,
                    want.binary_version,
                ))
                break
            else:
                logger.warning(
                    'Unable to install -dbgsym package: %s_%s from %s',
                    want.qualified_dbgsym_package,
                    want.binary_version,
                    want.source,
                )
                missing_writer.write('{}\t{}\n'.format(
                    want.qualified_dbgsym_package,
                    want.binary_version,
                ))

                for binary_stanza in same_name:
                    logger.info('Available version:\n%s', binary_stanza)

    options = ['-y', '-m']

    if args.debug:
        options.append('-oDebug::pkgDepCache::AutoInstall=true')
        options.append('-oDebug::pkgDepCache::Marker=true')
        options.append('-oDebug::pkgPolicy=true')
        options.append('-oDebug::pkgProblemResolver=true')
        options.append('-oDebug::pkgProblemResolver::ShowScores=true')

    subprocess.run(
        in_chroot + ['apt-get'] + options + ['install'] + list(to_get),
        check=True,
    )


if __name__ == '__main__':
    if sys.stderr.isatty():
        try:
            import colorlog
        except ImportError:
            pass
        else:
            formatter = colorlog.ColoredFormatter(
                '%(log_color)s%(levelname)s:%(name)s:%(reset)s %(message)s')
            handler = logging.StreamHandler()
            handler.setFormatter(formatter)
            logging.getLogger().addHandler(handler)
    else:
        logging.basicConfig()

    logging.getLogger().setLevel(logging.DEBUG)

    try:
        main()
    except KeyboardInterrupt:
        raise SystemExit(130)
    except subprocess.CalledProcessError as e:
        logger.error('%s', e)
        raise SystemExit(1)
